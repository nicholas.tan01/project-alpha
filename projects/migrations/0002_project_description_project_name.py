# Generated by Django 4.0.3 on 2022-03-30 19:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("projects", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="project",
            name="description",
            field=models.TextField(default="N/A"),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="project",
            name="name",
            field=models.CharField(default="N/A", max_length=200),
            preserve_default=False,
        ),
    ]
